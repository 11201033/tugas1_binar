import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class utama {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        BufferedReader input2 = new BufferedReader(new InputStreamReader(System.in));
        int i = 0;
        int hargatotal = 0;
        int penampung = 0;
        String menu[] = new String[100];
        int harga[] = new int[100];
        String keputusan = "Y";

        System.out.println("\n\033[33m=== My Cafetaria ===");
        System.out.println("----------------------------- ");
        System.out.println("\033[34mDaftar Menu Makanan ");
        System.out.println("----------------------------- ");
        System.out.println("\033[35m1. Mie Ayam        = Rp 15.000 ");
        System.out.println("2. Roti Kadet keju = Rp 5.000 ");
        System.out.println("3. Bakso           = Rp 15.000 ");
        System.out.println("4. Sup Iga         = Rp.12.000");

        while (keputusan.equals("Y") || keputusan.equals("y")) {
            System.out.print("\033[33mPilihan menu anda  = ");
            int pil = input.nextInt();
            if (pil == 1) {
                menu[i] = "Mie Ayam";
                harga[i] = 15000;
            } else if (pil == 2) {
                menu[i] = "Roti Kadet Keju";
                harga[i] = 5000;
            } else if (pil == 3) {
                menu[i] = "Bakso";
                harga[i] = 1500;
            } else if (pil == 4) {
                menu[i] = "Sup Iga";
                harga[i] = 12000;
            } else {
                System.out.println("\033[31mMaaf pilihan menu anda tidak tepat ");
                menu[i] = "\033[31mTidak Ada";
            }
            System.out.println("\033[34mMenu yang anda pesan adalah          : " + menu[i]);
            System.out.println("Harga yang harus dibayar             : " + harga[i]);
            System.out.print("Apakah anda ingin memesan lagi ? Y/T : ");
            try {
                keputusan = input2.readLine();
            } catch (IOException e) {
                System.out.println("\033[31mGagal Membaca Keyboard");
            }
            i++;
        }
        System.out.println("");
        System.out.println("\033[34mMenu yang anda pesan adalah          : " + i);
        for (int a = 0; a < i; a++) {
            System.out.print(menu[a] + ", ");
        }
        System.out.println("");
        for (int b = 0; b < i; b++) {
            hargatotal = hargatotal + harga[b];
        }
        System.out.println("\033[34mTotal yang harus anda bayar adalah   : Rp." + hargatotal);
    }
}
